package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;

import chapter6.beans.User;
import chapter6.dao.UserDao;
import chapter6.utils.CipherUtil;

public class UserService {
    public void insert(User user) {

        Connection connection = null;
        try {
            // 	UserDaoでインサートする前にパスワード暗号化をしないといけない。
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            connection = getConnection();
            new UserDao().insert(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    //ログインする。データベースに登録されているパスワードは暗号化されているから同じ形式で暗号化してからUserDaoに渡して選んできてもらう。
    public User select(String accountOrEmail, String password) {

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(password);

            connection = getConnection();
            User user = new UserDao().select(connection, accountOrEmail, encPassword);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    //ユーザー情報を更新したい場合に対象のユーザーをとってくる
    public User select(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();
            User user = new UserDao().select(connection, userId);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    //ユーザー情報を更新する際にまずパスワードを暗号化してからUSerDaoでアプデしてもらう。
    public void update(User user) {

        Connection connection = null;
        try {
        	if(!user.getPassword().isEmpty()) {
	            // パスワード暗号化
	            String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);
        	}

            connection = getConnection();
            new UserDao().update(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //登録、または更新したいユーザーの＠アカウントをUerDaoに取ってきてもらう。
    public User select(String account) {

        Connection connection = null;
        try {
            connection = getConnection();
            User user = new UserDao().select(connection, account);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}