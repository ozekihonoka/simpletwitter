package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("loginUser");

        User user = new UserService().select(loginUser.getId());

        request.setAttribute("user", user);
        request.getRequestDispatcher("setting.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        //設定画面でポストされてきたユーザー情報をgetUserメソッドでSettingUserに格納。
        User settingUser = getUser(request);
        //上のユーザー情報のうち＠アカウントを引数にしてかぶりがないか判定。
        //かぶりがなく問題なく登録できる場合はnull、あった場合はUser型が一つ帰ってくる。
        User firstUser = new UserService().select(settingUser.getAccount());

        if (isValid(settingUser, firstUser, errorMessages)) {
            try {
                new UserService().update(settingUser);
            } catch (NoRowsUpdatedRuntimeException e) {
                errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
            }
        }

        if (errorMessages.size() != 0) {
            request.setAttribute("errorMessages", errorMessages);
            //ここちょっと不明！！うまくいかなかったら確認してね！！！
            request.setAttribute("user", settingUser);
            request.getRequestDispatcher("setting.jsp").forward(request, response);
            return;
        }

        session.setAttribute("loginUser", settingUser);
        response.sendRedirect("./");
    }

    //新しいアカウント名などをUser型に格納するメソッド。
    private User getUser(HttpServletRequest request) throws IOException, ServletException {
    	User user = new User();

	    user.setId(Integer.parseInt(request.getParameter("id")));
	    user.setName(request.getParameter("name"));
	    user.setAccount(request.getParameter("account"));
	    user.setPassword(request.getParameter("password"));
	    user.setEmail(request.getParameter("email"));
	    user.setDescription(request.getParameter("description"));

	    return user;
    }
    //バリデーションの追加
    private boolean isValid(User settingUser, User firstUser, List<String> errorMessages) {

        String name = settingUser.getName();
        String account = settingUser.getAccount();
        String email = settingUser.getEmail();

        if (!StringUtils.isBlank(name) && (20 < name.length())) {
            errorMessages.add("名前は20文字以下で入力してください");
        }
        if (StringUtils.isBlank(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if (20 < account.length()) {
            errorMessages.add("アカウント名は20文字以下で入力してください");
        }
        if(firstUser != null) {
        	 errorMessages.add("アカウント名が重複しています");
        }
        if (!StringUtils.isBlank(email) && (50 < email.length())) {
            errorMessages.add("メールアドレスは50文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }


}
